FROM alpine:latest
ARG MYSQL_VERSION=8.0.17
ARG MYSQL_CON_VERSION=8.0.17
ARG GRPC_VERSION=v1.22.x
ARG PROTO_VERSION=v3.9.1

RUN apk update && \
    apk upgrade && \
    apk --update add \
        alpine-sdk \
        autoconf \
        automake \
        gcc \
        g++ \
        build-base \
        boost \
        boost-dev \
        ncurses-dev \
        ncurses-libs \
        cmake \
        xz \
        xz-dev \
        libtirpc \
        libtirpc-dev \
        rpcgen \
        bash \
        libtool \
        curl \
        unzip \
        git \
        bison \
        mysql \
        mysql-client \
        libstdc++ && \
    rm -rf /var/cache/apk/*



# mysql connector need ssl 1.0.0 and grpc need gcc 7
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/v3.8/main' >> /etc/apk/repositories
RUN apk update
RUN apk add openssl-dev==1.0.2r-r0
RUN apk add gcc==6.4.0-r9


#BUILD GRPC AND PROTOBUF FOR C++


WORKDIR /tmp

RUN git clone -b $PROTO_VERSION https://github.com/protocolbuffers/protobuf.git && \
cd protobuf && \
git submodule update --init --recursive && \
./autogen.sh && \
./configure && \
make && \
sudo make install

# Build gRPC
WORKDIR /tmp

RUN git clone -b $GRPC_VERSION https://github.com/grpc/grpc && \
cd grpc && \
git submodule update --init && \
make && \
sudo make install

# go back to gcc 8
RUN sudo rm -rf /etc/apk/repositories
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/latest-stable/main' >> /etc/apk/repositories
RUN apk update
RUN apk add --upgrade gcc
RUN apk add gcc==8.3.0-r0

RUN gcc --version

WORKDIR /tmp

RUN git clone https://github.com/jbeder/yaml-cpp.git && \
cd yaml-cpp && \
mkdir build && \
cd build && \
cmake .. && \
make && \
sudo make install


WORKDIR /tmp

RUN git clone https://github.com/Cylix/cpp_redis.git && \
cd cpp_redis  && \
git submodule init && git submodule update  && \
mkdir build && cd build  && \
cmake .. -DCMAKE_BUILD_TYPE=Release  && \
make  && \
sudo make install


RUN git clone https://github.com/mysql/mysql-connector-cpp.git && \
cd mysql-connector-cpp && \
git submodule update --init && \
mkdir build && \
cd build  && \
wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64.tar.xz && \
tar -xf mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64.tar.xz  && \
sudo cp -rf mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64/include/* /usr/include/ && \
sudo cp -rf mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64/lib/* /usr/lib/ && \
cmake --config -DCMAKE_INSTALL_LIBDIR=/usr/local/lib/ -DWITH_SSL=lib -DMYSQL_LIB_DIR=mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64/lib -DMYSQL_INCLUDE_DIR=mysql-${MYSQL_VERSION}-linux-glibc2.12-x86_64/include -DCMAKE_BUILD_TYPE=Release -DWITH_JDBC=TRUE -DBUILD_STATIC=ON -DCMAKE_INSTALL_PREFIX:PATH=/usr/local .. && \
make && \
sudo make install

RUN git clone https://github.com/oktal/pistache.git
RUN cd pistache && git submodule update --init && mkdir build && cd build && cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release .. && make && sudo make install

#clean up imageMM
#RUN rm -rf mysql-server-mysql-${MYSQL_VERSION}
RUN rm -rf mysql-connector-cpp
RUN rm -rf pistache
RUN rm -rf yaml-cpp
RUN rm -rf cpp_redis
RUN rm -rf tmp/*
RUN rm -rf /tmp/*